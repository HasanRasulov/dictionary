#include "../Headers/Dictionary.h"

Dictionary::Dictionary() {

	count = 0;
	size = 3;
	container = (char***) malloc(sizeof(char**) * size);
}

Dictionary::Dictionary(size_t size) {

	count = 0;
	this->size = size;
	container = (char***) malloc(sizeof(char**) * size);

}

Dictionary::~Dictionary() {

	for (size_t i = 0; i < size; i++) {
		free(container[i]);
	}
	free(container);

}

size_t Dictionary::getCount() const {
	return count;
}

size_t Dictionary::getSize() const {
	return size;
}

void Dictionary::enlargeDictionary(size_t size) {
	if (size < this->size) {
		puts("Operation failed.Because parameter less than current value ");
		return;
	}
	this->size = size;
	container = (char***) realloc(container, sizeof(char**) * size);

}

void Dictionary::printDictionary(void) {
	for (unsigned i = 0; i < count; i++) {
		for (unsigned j = 0; container[i][j]!=nullptr; j++) {
			printf("%s--", container[i][j]);
		}
		printf("\n");
	}

}

void Dictionary::addWordAndDefinitions(size_t numberofWords...) {



	if(count >= size) {
		size*=3;
		container = (char***)realloc(container,sizeof(char**)*size);

	}

	va_list args;
	va_start(args,numberofWords);

	container[count] = (char**)malloc(sizeof(char*)*numberofWords+1);

	size_t iter=0;

	while(iter < numberofWords) {

		container[count][iter] = va_arg(args,char*);

		++iter;
	}

	container[count][iter]=nullptr;

	count++;

	va_end(args);

}

void Dictionary::shrinkToFit(void) {

	size = count;
	container = (char***) realloc(container, sizeof(char**) * size);

}

void Dictionary::addWordAndDef(char* word, char* def) {

	if (count >= size) {
		size *= 3;
		container = (char***) realloc(container, sizeof(char**) * size);

	}

	container[count] = (char**) malloc(sizeof(char*) * 2);
	container[count][0] = word;
	container[count][1] = def;
	count++;
}

