#ifndef DICTIONARY_H_
#define DICTIONARY_H_

#include<stdio.h>
#include<malloc.h>
#include<cstdarg>
class Dictionary {

	size_t size;
	size_t count;
	char*** container;

public:



	Dictionary();
	Dictionary(size_t);
	~Dictionary();

	size_t getCount()const;
	size_t getSize()const;
	void enlargeDictionary(size_t);

	void printDictionary(void);
	void addWordAndDefinitions(size_t numberofWords...);

	void addWordAndDef(char* word,char* def);//ForTest

	void shrinkToFit(void);
};

#endif
