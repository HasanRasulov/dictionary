#ifndef MAINHEADER_H
#define MAINHEADER_H

#ifdef __cplusplus
extern "C" {
#endif

#include<stdbool.h>
#include<stdio.h>
#include<math.h>
#include<malloc.h>
#include<string.h>
#define show(x) for(int i=0;i<strlen(x);i++) printf("%c",x[i])

size_t length(char a[]);
void swap(int* i, int* j);
char* reverse(char* a);
_Bool isPrime(int a);
unsigned sumOfNumber(int a);
unsigned MutOfNumber(int a);
int abs(int a);
int reverseOfNumber(int a);
unsigned numberOfdigit(int a);
void everyDigit(int a);
char* mstrcpy(char* source, char* dest);
int mystrcmp(char* str1,char* str2 );


 struct Person {
    int yas;
    char* ad;
    char* soyad;
};



#ifdef __cplusplus
}
#endif

#endif /* MAINHEADER_H */

